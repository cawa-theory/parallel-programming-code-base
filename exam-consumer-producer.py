import logging
from os import listdir
from threading import Lock, Thread

# lock to protect the shared resource
lock = Lock()
to_stop = False


# Own implementation of a queue with lock to protect the shared resource
class Queue:
    def __init__(self):
        self.__list = []
        self.__lock = Lock()

    def push(self, message):
        with self.__lock:
            self.__list.insert(0, message)

    def pop(self):
        with self.__lock:
            return self.__list.pop()

    # get the list of files in the queue by copying the list
    def get_files(self):
        with self.__lock:
            return self.__list.copy()


# count the number of lines in a file based on a condition
def count_lines(file, condition):
    # Try to open the file and read it
    try:
        with open(file, 'r') as f:
            return len([line for line in f.readlines() if condition(line)])
    except Exception as e:
        # raise an exception if the file can't be read
        raise Exception(f'Error while reading file {file}: {e}')


# get only the name of the file
def get_file_name(file):
    return file.split('/')[-1]


def producer(path, queue):
    for name in listdir(path):
        # only push python files
        if name.endswith('.py'):
            queue.push(f'{path}/{name}')


def consumer(queue):
    while True:
        try:
            message = queue.pop()
        except:
            with lock:
                if to_stop:
                    break
            message = None

        if message is not None:
            # log the numbers of files in the queue
            logging.info(f'Number of files in the queue: {len(queue.get_files())}')

            # log the number of lines for each file
            logging.info(f'Number of lines for file {get_file_name(message)}: {count_lines(message, lambda x: True)}')

            # log the number of comments and white lines for each file
            logging.info(
                f'Number of comments for file {get_file_name(message)}: {count_lines(message, lambda x: x.startswith("#"))}')

            # log the number of white lines for each file
            logging.info(
                f'Number of white lines for file {get_file_name(message)}: {count_lines(message, lambda x: x == "\n")}')


if __name__ == '__main__':
    FORMAT = "%(asctime)s;%(levelname)s;%(thread)s;%(threadName)s;%(message)s"
    filename = "exam-producer-consumer.log"
    logging.basicConfig(format=FORMAT, filename=("%s" % filename),
                        level=logging.DEBUG)

    queue = Queue()
    fullpathdir = r'/Users/sachamouchon/Desktop/M2 INGE AL/S2/PROGRAMMATION PARALLÈLE/SOURCE CODE/code-base/'
    threads = [
        Thread(target=producer, args=(
            (r'%s' % fullpathdir), queue)),
        Thread(target=consumer, args=(queue,)),
        Thread(target=consumer, args=(queue,)),
        Thread(target=consumer, args=(queue,)),
    ]

    # start the threads
    [t.start() for t in threads]

    # wait for the first producer to finish
    [t.join() for t in threads[:1]]

    # signal the producers to stop
    with lock:
        to_stop = True
        # log the end of the producer
        logging.info('Producer has finish his work !')

    # wait for the other consumers to finish
    [t.join() for t in threads[1:]]

    # log the end of the program
    logging.info('I have finish my work boss !')
