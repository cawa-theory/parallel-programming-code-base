"""
@author: cawa-dev
"""
import logging
from random import randint
from threading import Lock, Thread


class Queue:
    def __init__(self):
        self.__list = []
        self.__lock = Lock()

    def push(self, msg):
        with self.__lock:
            self.__list.insert(0, msg)

    def pop(self):
        with self.__lock:
            return self.__list.pop()

    def is_empty(self):
        with self.__lock:
            return len(self.__list) == 0


referee_queue_ = Queue()
player_one_queue = Queue()
player_two_queue = Queue()


def player(id, own_queue, other_queue, referee_queue):
    while True:
        try:
            msg = own_queue.pop()
        except:
            continue

        if msg == "STOP":
            logging.info(f"Player {id} stopped")
            break
        if msg == 1:
            referee_queue.push(("WON", id))
            logging.info(f"Player {id}: send WON to referee")
            continue

        if msg % 2 == 0:
            number = msg // 2
        else:
            number = (3 * msg + 1) // 2
        other_queue.push(number)
        logging.info(f"Player {id}: send {number} to other player")


def referee(own_queue, player_one_queue, player_two_queue):
    random_number = randint(10000, 100000)
    # start_queue = player_one_queue if randint(0, 1) == 0 else player_two_queue
    start_queue = [player_one_queue, player_two_queue][randint(0, 1)]
    logging.info(f"Referee: start game")
    start_queue.push(random_number)

    while True:
        try:
            message, id = own_queue.pop()
            break
        except:
            continue

    logging.info(f"Referee: player {id} is winner")
    player_one_queue.push("STOP")
    player_two_queue.push("STOP")


if __name__ == "__main__":
    # Basic config for logging
    FORMAT = "%(asctime)s;%(levelname)s;%(threadName)s;%(message)s"
    logging.basicConfig(format=FORMAT, filename="players.log", level=logging.DEBUG)

    # Instantiate the threads
    player_one = Thread(target=player, args=(1, player_one_queue, player_two_queue, referee_queue_))
    player_two = Thread(target=player, args=(2, player_two_queue, player_one_queue, referee_queue_))
    referee = Thread(target=referee, args=(referee_queue_, player_one_queue, player_two_queue))

    # Start the threads
    player_one.start()
    player_two.start()
    referee.start()

    # Wait for the threads to finish
    player_one.join()
    player_two.join()
    referee.join()

    logging.info("Game finished !")
    logging.shutdown()
