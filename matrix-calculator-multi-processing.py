"""
Exemple using multiprocessing with multi-processes
auhtor: cawa-dev
"""
from multiprocessing import Pool


def mat_mul(
        mat1,
        mat2,
        res,
        start,
        end
):
    n = len(mat1)
    print(start, end)

    for i in range(start, end):
        for j in range(n):
            for k in range(n):
                res[i][j] += mat1[i][k] * mat2[k][j]
    return res


if __name__ == "__main__":
    N = 100
    A = [
        [
            (2 if i == j else 0)
            for j in range(N)
        ]
        for i in range(N)
    ]
    B = [
        [
            (3 if i == j else 0)
            for j in range(N)
        ]
        for i in range(N)
    ]
    C = [[0] * N for i in range(N)]
    print(f"{A=}")
    print(f"{B=}")

    nb_threads = 4

    with Pool(nb_threads) as p:
        p.starmap(
            mat_mul,
            [
                (A, B, C, i, i + N // nb_threads)
                for i in range(0, N, N // nb_threads)
            ]
        )
    # threads = [
    #     Process(target=mat_mul,
    #             args=(A, B, C, i, i + N // nb_threads))
    #     for i in range(0, N, N // nb_threads)
    # ]
    # [t.start() for t in threads]
    # [t.join() for t in threads]
    # for i in range(0, N, 4):
    #     mat_mul(A, B, C, i, i + N//nb_threads)

    print(f"{C=}")

# Exemple using multiprocessing with One process
#
# @author: cawa-dev
# """
# from multiprocessing import Process
#
#
# def mat_mul(
#         mat1,
#         mat2,
#         res,
#         start,
#         end
# ):
#     n = len(mat1)
#     print(start, end)
#
#     for i in range(start, end):
#         for j in range(n):
#             for k in range(n):
#                 res[i][j] += mat1[i][k] * mat2[k][j]
#     return res
#
#
# if __name__ == "__main__":
#     N = 100
#     A = [
#         [
#             (2 if i == j else 0)
#             for j in range(N)
#         ]
#         for i in range(N)
#     ]
#     B = [
#         [
#             (3 if i == j else 0)
#             for j in range(N)
#         ]
#         for i in range(N)
#     ]
#     C = [[0] * N for i in range(N)]
#     print(f"{A=}")
#     print(f"{B=}")
#
#     nb_threads = 4
#
#     threads = [
#         Process(target=mat_mul,
#                args=(A, B, C, i, i + N // nb_threads))
#         for i in range(0, N, N // nb_threads)
#     ]
#     [t.start() for t in threads]
#     [t.join() for t in threads]
#     # for i in range(0, N, 4):
#     #     mat_mul(A, B, C, i, i + N//nb_threads)
#
#     print(f"{C=}")

# My own implementation of matrix calculator
# """
# @author: cawa-dev
# """
#
# from random import randint
#
# N = 10
# MATRIX_A = [
#     [randint(1, 10) for _ in range(N)] for _ in range(N)
# ]
# MATRIX_B = [
#     [randint(1, 10) for _ in range(N)] for _ in range(N)
# ]
# MATRIX_C = [
#     [0] * N for _ in range(N)
# ]
#
# if __name__ == "__main__":
#     for i in range(N):
#         for j in range(N):
#             s = 0
#             for k in range(N):
#                 s += MATRIX_A[i][k] * MATRIX_B[k][j]
#             MATRIX_C[i][j] = s
