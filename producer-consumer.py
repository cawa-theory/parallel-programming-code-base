"""
@author: cawa-dev
"""
from threading import Lock, Thread


class Queue:
    def __init__(self):
        self.__list = []
        self.__lock = Lock()

    def push(self, msg):
        with self.__lock:
            self.__list.insert(0, msg)

    def pop(self):
        with self.__lock:
            return self.__list.pop()

    def is_empty(self):
        with self.__lock:
            return len(self.__list) == 0


q = Queue()


def producer(id, count, text):
    for i in range(count):
        msg = f"P{id}: msg {i}: {text}"
        q.push(msg)


to_stop = False


def consumer(filename):
    with open(filename, "w") as file:
        while not to_stop or not q.is_empty():
            try:
                msg = q.pop()
            except:
                continue
            file.write(msg)
            file.write("\n")


if __name__ == "__main__":
    p1 = Thread(target=producer, args=(1, 10000, "Je suis un développeur"))
    p2 = Thread(target=producer, args=(2, 10000, "Je ne suis pas un développeur"))
    c = Thread(target=consumer, args=("msg.txt",))

    c.start()
    p1.start()
    p2.start()

    p2.join()
    p1.join()
    print("Producer stopped !")
    to_stop = True

    c.join()
    print("Finished !")

# My Solution
# from queue import Queue
# from threading import Thread, Lock
#
# lock = Lock()
# message_list = ["Je suis un développeur", "Je ne suis pas un développeur"]
#
#
# def producer(count, q):
#     for i in range(count):
#         with lock:
#             q.put(message_list[i])
#     q.put(None)
#
#
# def consumer(q):
#     while True:
#         with lock:
#             data = q.get()
#         if data is not None:
#             print(data)
#         else:
#             break
#
#
# if __name__ == "__main__":
#     q = Queue()
#     threads = [Thread(target=producer, args=(q,)), Thread(target=consumer, args=(q,))]
#     for t in threads:
#         t.start()
#     for t in threads:
#         t.join()
#     print("\nthreads finished ")
