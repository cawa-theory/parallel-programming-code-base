"""
@author: cawa-dev
"""

# Solution without Mutex

from threading import Thread


def myFunction(count, text):
    for i in range(count):
        print(text, end="")


if __name__ == "__main__":
    DATA = [(2000, "0"), (1000, "1"), (1200, "2")]

    threads = [Thread(target=myFunction, args=d) for d in DATA]
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    print("\nthreads finished ")

# --------------------------------------------------------------
# First solution
# from threading import Thread, Lock
# lock = Lock()
#
# def myFunction(count, text):
#     with lock:
#         for i in range(count):
#             print(text, end="")
#
#
# if __name__ == "__main__":
#     DATA = [(2000, "0"), (1000, "1"), (1200, "2")]
#
#     threads = [Thread(target=myFunction, args=d) for d in DATA]
#     for t in threads:
#         t.start()
#     for t in threads:
#         t.join()
#     print("\nthreads finished ")
#
# --------------------------------------------------------------
#
# Second solution
# from threading import Thread, Lock
#
# lock = Lock()
#
# def myFunction(count, text):
#     for i in range(count):
#         with lock:
#             print(text, end="")
#
#
# if __name__ == "__main__":
#     DATA = [(2000, "0"), (1000, "1"), (1200, "2")]
#
#     threads = [Thread(target=myFunction, args=d) for d in DATA]
#     for t in threads:
#         t.start()
#     for t in threads:
#         t.join()
#     print("\nthreads finished ")
