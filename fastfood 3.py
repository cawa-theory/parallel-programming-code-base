# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:50:43 2022

@author: boula
"""

import asyncio
from datetime import datetime

soda_lock = None
burger_sem = None
fries_lock = None


def log(msg):
    print(datetime.now(), msg)


async def get_soda(client):
    # Acquisition du verrou. La syntaxe 'async with FOO' 
    # peut etre lue comme 'with (yield from FOO)’
    log(F"    > Remplissage du soda pour {client}")
    async with soda_lock:
        log(F"    apres lock soda pour {client}")
        # Une seule tache a la fois peut executer ce bloc
        await asyncio.sleep(1)
        log(F"    < Le soda de {client} est pret")


fries_counter = 0


async def get_fries(client):
    global fries_counter
    log(F"    > Recuperation des frites pour {client}")
    async with fries_lock:
        log(F"    apres lock fries pour {client}")
        if fries_counter == 0:
            log("   ** Demarrage de la cuisson des frites")
            await asyncio.sleep(4)
            fries_counter = 5
            log("   ** Les frites sont cuites")
        await asyncio.sleep(1)
        fries_counter -= 1
        log(F"    < Les frites de {client} sont pretes")


async def get_burger(client):
    now = datetime.now()
    log(F"    > Commande du burger en cuisine pour {client}")
    async with burger_sem:
        now = datetime.now()
        log(F"    apres sem burger pour {client}")
        await asyncio.sleep(3)
        log(F"    < Le burger de {client} est pret")


async def serve(client):
    log(F"=> Commande passee par {client}")
    start_time = datetime.now()
    await asyncio.gather(
        *[
            get_soda(client),
            get_fries(client),
            get_burger(client)
        ]
    )
    total = datetime.now() - start_time
    log(F"<= {client} servi en {total}")
    return total


async def launch():
    global soda_lock, burger_sem, fries_lock
    soda_lock = asyncio.Lock()
    burger_sem = asyncio.Semaphore(3)
    fries_lock = asyncio.Lock()

    await asyncio.gather(*[serve(c) for c in "ABCDEFGH"])


asyncio.run(launch(), debug=True)
