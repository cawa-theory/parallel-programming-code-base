"""
@author: cawa-dev
"""

import logging
from datetime import datetime
from enum import Enum
from random import randint
from threading import Thread, Lock
from time import sleep

TO_STOP = False

lock = Lock()
forks = [1] * 5


class Status(Enum):
    THINKING = 0
    STARVING = 1
    EATING = 2


class Philosopher(Thread):
    def __init__(self, id):
        Thread.__init__(self)
        self.__id = id
        self.status = Status.THINKING

    def run(self):
        global TO_STOP
        while not TO_STOP:
            if self.status == Status.THINKING:
                t = randint(1, 10)
                logging.info(f"Philosopher({self.__id}) thinks for {t}s")
                sleep(t)
                self.status = Status.STARVING
            elif self.status == Status.STARVING:
                logging.info(f"Philosopher({self.__id}) is starving")
                start = datetime.now()
                while (datetime.now() - start).total_seconds() < 20:
                    with lock:
                        if forks[self.__id] and forks[(self.__id + 1) % 5]:
                            forks[self.__id] = 0
                            forks[(self.__id + 1) % 5] = 0
                            self.status = Status.EATING
                            break
                if self.status == Status.STARVING:
                    logging.error(f"Philosopher({self.__id}) is dead!")
                    TO_STOP = True
            elif self.status == Status.EATING:
                t = 5
                logging.info(f"Philosopher({self.__id}) is eating for {t}s")
                sleep(5)
                with lock:
                    forks[self.__id] = 1
                    forks[(self.__id + 1) % 5] = 1
                self.status = Status.THINKING


if __name__ == "__main__":
    logging.basicConfig(
        filename="philosophers.log",
        format="%(asctime)-15s;%(thread)d;%(levelname)s;%(filename)s;%(lineno)d;%(message)s",
        level=logging.DEBUG)

    philosophers = [Philosopher(i) for i in range(5)]
    [t.start() for t in philosophers]
    [t.join() for t in philosophers]
    print("finished")
    logging.shutdown()
