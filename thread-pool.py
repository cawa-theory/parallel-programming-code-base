"""
@author: cawa-dev
"""

from queue import Queue
from random import randint
from threading import Thread
from time import sleep


def task(id, timer):
    print(F"Start task {id} : {timer}")
    sleep(timer)
    print(F"End task {id} : {timer}")


class ThreadInPool(Thread):
    def __init__(self, id, tasklist):
        Thread.__init__(self)
        self.__id = id
        self.__tasklist = tasklist

    def run(self):
        print(F"Start thread {self.__id}")

        while True:
            # get an action in the queue
            try:
                fct, arguments = self.__tasklist.get(block=False)
            except:
                break

            # execute the function
            print(F"Thread {self.__id} : start launching {fct.__name__}{arguments}")
            fct(*arguments)
            print(F"Thread {self.__id} : finish {fct.__name__}{arguments}")

        print(F"End thread {self.__id}")


class ThreadPool:
    def __init__(self, thread_count=4):
        self.__queue = Queue()
        self.threads = [ThreadInPool(i, self.__queue) \
                        for i in range(thread_count)]

    @property
    def Queue(self):
        return self.__queue

    def start(self):
        print("Start the threads")
        for t in self.threads: t.start()

    def join(self):
        for t in self.threads: t.join()
        print("Join all the threads: success")


if __name__ == "__main__":
    pool = ThreadPool()
    for i in range(50):
        pool.Queue.put((task, (i, randint(1, 2000) / 1000)))

    pool.start()
    pool.join()
