# -*- coding: utf-8 -*-
"""
Created on Fri May 13 11:14:24 2022

@author: boula
"""

from datetime import datetime
from os import scandir
from shutil import move
from threading import Lock, Thread
from time import sleep

"""
3 répertoires
    - arrivee
    - docx
    - xlsx

1 producteur:
    scanne 'arrivee' et envoie les messages 
    avec le fichier au bon consommateur

2 consommateurs:
    - consommateur docx
    - consommateur xslx
"""

PATH_ARRIVEE = "/arrivee"
PATH_DOCX = "/docx"
PATH_XLSX = "/xlsx"

lock_docx = Lock()
queue_docx = []

lock_xlsx = Lock()
queue_xlsx = []


def producer(path, queue_docx, queue_xlsx):
    global lock_docx, lock_xlsx

    while True:
        with scandir(path) as S:
            for entry in S:
                if entry.name.endswith(".docx") or entry.name.endswith(".doc"):
                    with lock_docx:
                        queue_docx.insert(0, entry.name)
                elif entry.name.endswith(".xlsx") or entry.name.endswith(".xls"):
                    with lock_xlsx:
                        queue_xlsx.insert(0, entry.name)
        sleep(3)


def consumer(inpath, outpath, lock, queue):
    while True:
        # get filename
        with lock:
            try:
                filename = queue.pop()
            except:
                filename = None

        # get timestamp
        now = datetime.now()
        osfilename = F"{filename}_{now.year}__{now.month}_{now.day}_{now.hour}_{now.second}_{now.microsecond}"

        # move
        if filename:
            move(inpath + "/" + filename, outpath + "/" + osfilename)


if __name__ == "__main__":
    threads = [Thread(target=producer, args=(PATH_ARRIVEE, queue_docx, queue_xlsx)),
               Thread(target=consumer, args=(PATH_ARRIVEE, PATH_DOCX, lock_docx, queue_docx)),
               Thread(target=consumer, args=(PATH_ARRIVEE, PATH_XLSX, lock_xlsx, queue_xlsx))]

    [t.start() for t in threads]
    [t.join() for t in threads]
