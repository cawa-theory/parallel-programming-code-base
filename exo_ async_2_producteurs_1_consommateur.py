# -*- coding: utf-8 -*-
"""
Created on Mon May 27 15:10:53 2024

@author: boula
"""

from asyncio import run, gather, Queue

from aiofile import async_open


async def producer(id, count, text):
    print(f'producer {id} started')
    for i in range(count):
        msg = f"P{id}: msg {i}: {text}"
        print(msg)
        await q.put(msg)
    await q.put(None)
    print(f'producer {id} stopped')


async def consumer(nb_prod, filename):
    print("consumer starts")
    async with async_open(filename, "w") as file:
        while nb_prod != 0 or not q.empty():
            try:
                msg = q.get_nowait()
            except Exception as e:
                continue
            if msg is None:
                nb_prod -= 1
            else:
                await file.write(msg)
                await file.write("\n")
        print("consumer stopped")


async def launch():
    global q
    q = Queue()

    cotasks = [
        consumer(2, "msg-async.txt"),
        producer(1, 10000, "je suis un developpeur"),
        producer(2, 10000, "je ne suis pas un developpeur"),
    ]

    await gather(*cotasks)
    print("finished")


if __name__ == "__main__":
    run(launch())
