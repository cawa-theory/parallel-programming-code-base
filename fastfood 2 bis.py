# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:50:43 2022

@author: boula
"""

import asyncio
from datetime import datetime

soda_lock = asyncio.Lock()


async def get_soda(client):
    # Acquisition du verrou. La syntaxe 'async with FOO' 
    # peut être lue comme 'with (yield from FOO)’
    global soda_lock
    async with soda_lock:
        # Une seule tâche à la fois peut exécuter ce bloc
        print(F"    > Remplissage du soda pour {client}")
        await asyncio.sleep(1)
        print(F"    < Le soda de {client} est prêt")


fries_counter = 0
fries_lock = asyncio.Lock()


async def get_fries(client):
    global fries_counter, fries_lock
    async with fries_lock:
        print(F"    > Récupération des frites pour {client}")
        if fries_counter == 0:
            print("   ** Démarrage de la cuisson des frites")
            await asyncio.sleep(4)
            fries_counter = 5
            print("   ** Les frites sont cuites")
        fries_counter -= 1
        print(F"    < Les frites de {client} sont prêtes")


burger_sem = asyncio.Semaphore(3)


async def get_burger(client):
    global burger_sem
    print(F"    > Commande du burger en cuisine pour {client}")
    async with burger_sem:
        await asyncio.sleep(3)
        print(F"    < Le burger de {client} est prêt")


async def serve(client):
    print(F"=> Commande passee par {client}")
    start_time = datetime.now()
    await asyncio.wait(
        [
            asyncio.create_task(get_soda(client)),
            asyncio.create_task(get_fries(client)),
            asyncio.create_task(get_burger(client))
        ]
    )
    total = datetime.now() - start_time
    print(F"<= {client} servi en {total}")
    return total


async def launch():
    await asyncio.wait([asyncio.create_task(serve("A")),
                        asyncio.create_task(serve("B"))])


asyncio.run(launch(), debug=True)
# asyncio.run( launch() )
