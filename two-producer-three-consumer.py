"""
@author: cawa-dev
"""

from queue import Queue
from threading import Thread, Lock

lock = Lock()
to_stop = False


def producer(id, counter, queue):
    for i in range(1, counter + 1):
        queue.put(f'P{id}: -{i}/{counter}')


def consumer(id, queue, filename):
    with open(filename, 'w') as file:
        while True:
            try:
                message = queue.get_nowait()
            except:
                with lock:
                    if to_stop:
                        break
                message = None

            if message is not None:
                file.write(message)
                file.write('\n')
                file.flush()


if __name__ == '__main__':
    queue = Queue()
    threads = [
        Thread(target=producer, args=(1, 10000, queue)),
        Thread(target=producer, args=(2, 12000, queue)),
        Thread(target=consumer, args=(1, queue, 'consumer1.txt')),
        Thread(target=consumer, args=(2, queue, 'consumer2.txt')),
        Thread(target=consumer, args=(3, queue, 'consumer3.txt')),
    ]

    [t.start() for t in threads]

    [t.join() for t in threads[:2]]
    with lock:
        to_stop = True

    [t.join() for t in threads[2:]]

    print('Finished !')
