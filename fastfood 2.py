# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:50:43 2022

@author: boula
"""

import asyncio
from datetime import datetime


async def get_soda(client):
    print(F"    > Remplissage du soda pour {client}")
    await asyncio.sleep(1)
    print(F"    < Le soda de {client} est pret")


async def get_fries(client):
    print(F"    > Démarrage de la cuisson des frites pour {client}")
    await asyncio.sleep(4)
    print(F"    < Les frites de {client} sont pretes")


async def get_burger(client):
    print(F"    > Commande du burger en cuisine pour {client}")
    await asyncio.sleep(3)
    print(F"    < Le burger de {client} est pret")


async def serve(client):
    print(F"=> Commande passee par {client}")
    start_time = datetime.now()
    await asyncio.wait(
        [
            asyncio.create_task(get_soda(client)),
            asyncio.create_task(get_fries(client)),
            asyncio.create_task(get_burger(client))
        ]
    )
    total = datetime.now() - start_time
    print(F"<= {client} servi en {total}")
    return total


async def launch():
    await asyncio.wait([asyncio.create_task(serve("A")),
                        asyncio.create_task(serve("B"))])


asyncio.run(launch())
