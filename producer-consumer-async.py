"""
@author: cawa-dev
"""
from asyncio import gather, run, Queue

from aiofile import async_open


async def producer(id, count, text):
    print(f"Producer {id} started !")
    for i in range(count):
        msg = f"P{id}: msg {i}: {text}"
        print(msg)
        await q.put(msg)
    await q.put(None)
    print(f"Producer {id} stopped !")


async def consumer(number_of_producer, filename):
    print("Consumer started !")
    async with async_open(filename, "w") as file:
        while number_of_producer != 0 or not q.empty():
            try:
                msg = q.get_nowait()
            except Exception as e:
                continue
            if msg is None:
                number_of_producer -= 1
            else:
                await file.write(msg)
                await file.write("\n")
    print("Consumer stopped !")


async def launch():
    global q
    q = Queue()

    co_tasks = [
        consumer(2, "msg-async.txt"),
        producer(1, 1000, "Je suis un développeur"),
        producer(2, 1000, "Je ne suis pas un développeur"),
    ]

    await gather(*co_tasks)
    print("Finished !")


if __name__ == "__main__":
    run(launch())
