"""
@author: cawa-dev
"""

from concurrent.futures import ThreadPoolExecutor

path = [
    (0, 50),
    (10, 10),
    (95, 35),
    (70, 72),
    (27, 45),
    (42, 99),
    (16, 95),
    (50, 98),
    (100, 100)
]


def sign(x):
    if x < 0:
        return -1
    elif x > 0:
        return 1
    else:
        return 0


def compute_path(start, end):
    points = [start]
    current = start
    while current != end:
        dx = sign(end[0] - current[0])
        dy = sign(end[1] - current[1])
        next_point = (current[0] + dx, current[1] + dy)
        points.append(next_point)
        current = next_point

    return points


if __name__ == '__main__':
    with ThreadPoolExecutor(4) as executor:
        results = []
        for i in range(len(path) - 1):
            results.append(executor.submit(compute_path, path[i], path[i + 1]))

    final_path = results[0].result()

    for x in results[1:]:
        final_path += x.result()[1:]

    print(final_path)
