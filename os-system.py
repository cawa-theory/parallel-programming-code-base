if __name__ == "__main__":
    import os

    err = os.system("cd /tmp > /tmp/os-system.log 2>&1")

    if err != 0:
        raise Exception(f"Error: {err}")

    with open("/tmp/os-system.log", "r") as f:
        for line in f:
            print(line, end="")

# Exemple 1
# ------------------------------
# if __name__ == "__main__":
#     err = os.system("ls -l")
#     print(f"{err=}")
